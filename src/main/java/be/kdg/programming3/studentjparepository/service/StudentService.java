package be.kdg.programming3.studentjparepository.service;

import be.kdg.programming3.studentjparepository.domain.Student;

import java.util.List;
import java.util.Optional;


public interface StudentService {
    Student addStudent(Student student);
    List<Student> findStudents();
    Optional<Student> findStudent(int id);
    void changeStudent(Student student);
    void deleteStudent(int id);
}
