package be.kdg.programming3.studentjparepository.service;

import be.kdg.programming3.studentjparepository.domain.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{

    @Override
    public Student addStudent(Student student) {
        return null;
    }

    @Override
    public List<Student> findStudents() {
        return null;
    }

    @Override
    public Optional<Student> findStudent(int id) {
        return Optional.empty();
    }

    @Override
    public void changeStudent(Student student) {

    }

    @Override
    public void deleteStudent(int id) {

    }
}
